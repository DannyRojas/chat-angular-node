//instalamos las dependencias a utilizar

var express 	= require('express');
//mysql como motor de bd
var mysql 		= require('mysql');

//dependencia para parsear los datos recibidos
var bodyParser 	= require('body-parser')
var restapi 	= express();

//iniciamos el servior con socket
var server 		= require('http').Server(restapi);
var io 			= require('socket.io')(server);

//creamos la conexion a la bd con los datos de configuracion
var connection = mysql.createConnection({
   host: 'localhost',
   user: 'root',
   password: '',
   database: 'node_mysql',
   port: ''
});
connection.connect(function(error){
   if(error){
      throw error;
   }else{
      console.log('Conexion correcta.');
   }
});

//usamos esta funcion para poder recibir peticiones de Apps externas
restapi.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

restapi.use(express.static('public'));
restapi.use(bodyParser.urlencoded({ extended: true }))
restapi.use(bodyParser.json())

io.on('connection', function(socket) {
  console.log('Alguien se ha conectado con Sockets');

  	//preguntamos si hay mensajes para mostrar
    socket.on('emmit', function(data) {
      console.log(data)
      var idfriend = data[0].id;
      var myId = data[0].myid;
    	var query = connection.query('SELECT * FROM chat WHERE (id1 = ? || id2= ?) AND (id1 = ? || id2= ?) AND state=1 ORDER BY fecha_creacion DESC limit 0,1 ',[idfriend,idfriend,myId,myId],function(error, result){
              if(error){
                 console.log(error)
              }else{
                console.log(result);
                if(result.length>0){
                  token = result[0].token_chat;
                  connection.query('SELECT * FROM messagges WHERE token_chat = ? ORDER BY fecha_creacion ASC',[token],function(error, result){  
                    if(error){
                         console.log(error)
                      }else{
                     //   console.log(result);
                         socket.emit('messages', result);
                       }
                  });

                }else{
                  console.log('9')
                  
                }
            
              }
           });
    });
 
		//se ejecuta cuando hay un nuevo mensaje
  socket.on('new-message', function(data) {
  	console.log(data)
    var tok = parseInt(data[0].tokenchat);
    console.log(tok.length)
    if(tok=2222 && tok != undefined){
      console.log(1)
      connection.query('INSERT INTO messagges(text,author,token_chat) VALUES(?, ?, ?)', [data[0].text,data[0].author,data[0].tokenchat], function(error, result){
              if(error){
                  console.log(error)
              }else{
                  console.log({ "data" : result });
                  io.sockets.emit('messages', data);
              }
            });
    }else{
      console.log(2)
      connection.query('SELECT * FROM chat ',function(error, result){  
        if(error){
         console.log(error)
        }else{
          if(result.length >0){
             token_chat = result[0].token_chat + 1;
          }else{
            token_chat = 2222;
          }
           console.log("token",token_chat)
          connection.query('INSERT INTO chat(id1,id2,token_chat) VALUES(?, ?, ?)', [data[0].author,data[0].idfriend,token_chat], function(error, result){

            connection.query('INSERT INTO messagges(text,author,token_chat) VALUES(?, ?, ?)', [data[0].text,data[0].author,token_chat], function(error, result){
              if(error){
                  console.log(error)
              }else{
                  console.log({ "data" : result });
                  io.sockets.emit('messages', data);
              }
            });
          });
        }
      });
    } 

  });
});

restapi.get('/api/v1/insert', function(req, res){
    var query = connection.query('INSERT INTO personaje(nombre, apellido, biografia) VALUES(?, ?, ?)', ['Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Maggie.'], function(error, result){
      if(error){
         res.json({ "data" : error });
      }else{
         res.json({ "data" : result });

      }
    }
   );
});

restapi.post('/api/v1/validate', function(req, res){
	console.log("data:", req.body);
	var username = req.body.username
      var query = connection.query('SELECT * FROM users WHERE user = ?', [username], function(error, result){
            if(error){
               res.json({ "data" : false});
            }else{
               var resultado = result;
               if(resultado.length > 0){
                   res.json({ "data" : true, "id":resultado[0].id ,"foto":resultado[0].foto});
               }else{
                  res.json({ "data" : false });
               }
            }
         }
      );
});

restapi.post('/api/v1/friends', function(req, res){
  console.log("data:", req.body);
  var currentUser = req.body.currentUser
      var query = connection.query('SELECT * FROM friends,users WHERE friends.id_friend=users.id AND id_user = ? ORDER BY user ASC', [currentUser], function(error, result){
            if(error){
               res.json({ "data" : false});
            }else{
               var resultado = result;
             //  console.log(resultado)
               if(resultado.length > 0){
                var json = Array();
                    for(var i=0;i<resultado.length;i++){
                      json[i]={ "data" : true, "idfriend":resultado[i].id_friend ,"foto":resultado[i].foto,'state':resultado[i].state,'name':resultado[i].user};
                    }
                   res.json({"data":true,"json":json});
               }else{
                  res.json({ "data" : false });
               }
            }
         }
      );
});

restapi.post('/api/v1/Addfriend', function(req, res){
  console.log("data:", req.body);
  var currentUser   = req.body.currentUser
  var currentFriend = req.body.currentFriend
    connection.query('SELECT * FROM users WHERE user = ?', [currentFriend],function(error, result){
            if(error){
               res.json({ "data" : false});
            }else{
               var resultado = result;
               if(resultado.length <= 0){
                   res.json({ "data" : false,"msm":"Usuario no existe"});
               }else{
                idfriend=resultado[0].id;
                connection.query('SELECT * FROM friends WHERE id_user = ? AND id_friend=?', [currentUser,idfriend], function(error, result){
                  if(error){
                     res.json({ "data" : false});
                  }else{
                      var resultadoc = result;
                     if(resultadoc.length > 0){
                         res.json({ "data" : false,"msm":"Sois Amigos"});
                     }else{
                        
                        connection.query('INSERT INTO friends (id_friend,id_user) values (?,?)', [idfriend,currentUser],function(error, result){
                            if(error){
                               res.json({ "data" : false});
                            }else{
                              res.json({ "data" : true, "msm":"Se ha guardado tu nuevo amigo"});
                            }
                        });
                    }
                  
                 }
               });
            }
         }
      });
});
server.listen(3000);

console.log("Server lintening on port 3000");

