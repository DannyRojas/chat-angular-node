-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2018 at 05:34 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_mysql`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `token_chat` int(11) NOT NULL,
  `id1` bigint(20) NOT NULL,
  `id2` bigint(20) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(100) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`token_chat`, `id1`, `id2`, `fecha_creacion`, `state`) VALUES
(2222, 12421345, 12421346, '2018-08-17 06:09:19', '1');

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id_fr` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_friend` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`id_fr`, `id_user`, `id_friend`) VALUES
(2, 12421345, 12421348),
(3, 12421345, 12421346),
(4, 12421346, 12421345);

-- --------------------------------------------------------

--
-- Table structure for table `messagges`
--

CREATE TABLE `messagges` (
  `id` int(11) NOT NULL,
  `text` varchar(1222) NOT NULL,
  `author` varchar(100) NOT NULL,
  `token_chat` bigint(20) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(100) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messagges`
--

INSERT INTO `messagges` (`id`, `text`, `author`, `token_chat`, `fecha_creacion`, `state`) VALUES
(15, 'hola', '12421345', 2222, '2018-08-17 06:07:39', '1'),
(16, 'hola', '12421345', 2222, '2018-08-17 06:09:19', '1'),
(23, 'como esta?', '12421345', 2223, '2018-08-18 05:58:42', '1'),
(26, 'hola camilo como esta?', '12421345', 2222, '2018-08-18 06:03:08', '1'),
(27, 'bien y vos', '12421345', 2222, '2018-08-18 06:06:15', '1'),
(28, 'chao', '12421345', 2222, '2018-08-18 06:07:35', '1'),
(29, 'me voy', '12421345', 2222, '2018-08-18 06:08:12', '1'),
(30, 'bueno!', '12421345', 2222, '2018-08-18 06:08:57', '1'),
(31, 'chao', '12421345', 2222, '2018-08-18 06:10:57', '1'),
(32, 'no se que hcaer', '12421345', 2222, '2018-08-18 06:11:39', '1'),
(33, 'bueno', '12421345', 2222, '2018-08-18 06:12:46', '1'),
(34, 'add', '12421345', 2222, '2018-08-18 06:13:23', '1'),
(35, 'dasd', '12421345', 2222, '2018-08-18 06:13:59', '1'),
(36, 'sdsd', '12421345', 2222, '2018-08-18 06:14:24', '1'),
(37, '233', '12421345', 2222, '2018-08-18 06:15:10', '1'),
(38, 'sdads', '12421345', 2222, '2018-08-18 06:15:17', '1'),
(39, 'bueno', '12421345', 2222, '2018-08-18 06:26:18', '1'),
(40, 'bueno', '12421345', 2222, '2018-08-18 06:28:32', '1'),
(41, 'chaito', '12421345', 2222, '2018-08-18 06:30:50', '1'),
(42, 'chao me voy a dormir hablamos mañana', '12421345', 2222, '2018-08-18 07:29:12', '1'),
(43, 'bueno esta bien', '12421346', 2222, '2018-08-18 07:34:51', '1'),
(44, 'hablamos mañana entonces', '12421346', 2222, '2018-08-18 07:35:19', '1'),
(45, 'ok', '12421346', 2222, '2018-08-18 07:36:03', '1'),
(46, 'chao', '12421345', 2222, '2018-08-18 07:36:14', '1');

-- --------------------------------------------------------

--
-- Table structure for table `personaje`
--

CREATE TABLE `personaje` (
  `personaje_id` smallint(5) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `biografia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personaje`
--

INSERT INTO `personaje` (`personaje_id`, `nombre`, `apellido`, `biografia`) VALUES
(1, 'Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Maggie.'),
(2, 'Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Maggie.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user` varchar(13) NOT NULL,
  `foto` varchar(40) NOT NULL DEFAULT 'foto.png',
  `activated` int(1) NOT NULL DEFAULT '1',
  `state` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `foto`, `activated`, `state`) VALUES
(12421345, 'dannyrojas', 'foto.png', 1, ''),
(12421346, 'camilopuello', 'foto.png', 1, 'Dios eres lo mejor'),
(12421347, 'alejandroroa', 'foto.png', 1, ''),
(12421348, 'pedroleal', 'foto.png', 1, 'Amo jugar Futbol '),
(12421349, 'juanvilla', 'foto.png', 1, ''),
(12421350, 'gustavoser', 'foto.png', 1, ''),
(12421351, 'karolrojas', 'foto.png', 1, ''),
(12421352, 'tefaroa', 'foto.png', 1, ''),
(12421353, 'ramonama', 'foto.png', 1, ''),
(12421354, 'islapor', 'foto.png', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`token_chat`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id_fr`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_friend` (`id_friend`);

--
-- Indexes for table `messagges`
--
ALTER TABLE `messagges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personaje`
--
ALTER TABLE `personaje`
  ADD PRIMARY KEY (`personaje_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `token_chat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2223;
--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id_fr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `messagges`
--
ALTER TABLE `messagges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `personaje`
--
ALTER TABLE `personaje`
  MODIFY `personaje_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12421355;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
