import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { MenuItemsD } from './menu-items/menu-itemsD';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';


@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective
   ],
  providers: [ MenuItems,MenuItemsD ]
})
export class SharedModule { }
