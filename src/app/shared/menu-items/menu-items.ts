import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
    {state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'dashboard' },
    {state: 'friends', type: 'link', name: 'Friends', icon: 'people_outline'},
    {state: 'grid', type: 'link', name: 'Messages', icon: 'chat'}
    
];  

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}