import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}


const MENUITEMSD = [
  
  {state: 'lists', type: 'link', name: 'Trash', icon: 'delete'},
  {state: 'menu', type: 'link', name: 'Settings', icon: 'settings'}
  
]; 

@Injectable()


export class MenuItemsD {
  getMenuitemD(): Menu[] {
    return MENUITEMSD;
  }

}