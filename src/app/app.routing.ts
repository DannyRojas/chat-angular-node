import { Routes, RouterModule } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards';

export const AppRoutes: Routes = [
  { path: 'home', component: HomeComponent ,canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: '',component: FullComponent,canActivate: [AuthGuard],
      children: [{ 
        path: '', 
        redirectTo: 'dashboard', 
        pathMatch: 'full'
      }, {
        path: '',
        loadChildren: './material-component/material.module#MaterialComponentsModule'
      }
   ]}
];
export const routing = RouterModule.forRoot(AppRoutes);