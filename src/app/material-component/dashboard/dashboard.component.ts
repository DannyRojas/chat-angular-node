import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders,HttpParams  } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChatService} from './../chat.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  message: string;
  messages: string[] = [];
  messagess: Messagess[];

  constructor(private chatService: ChatService,private http: HttpClient){
    this.messagess = 
    [
        { 'id': '1001', 'text': 'Anil SIngh', 'author': 'http://www.code-sample.com' }
    ];
  }

  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  
  sendPhp(event){
    const body = new HttpParams()
          .set('num', "1");

    this.http.post('/api/v1/select',body,{
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    })
        .subscribe((data)=>{
            console.log('data success',data)
            console.log(data)
        },(error) =>{
          console.log('data error',error);
        })
  }

  sendMessage() {
    console.log(this.messagess);
    this.chatService.sendMessage(this.messagess);
    this.message = '';
  }

  ngOnInit() {
    
  }
  typesOfShoes = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  messagesx: any[] = []; 
    
  
  
}

export class Messagess {
  id: String;
  text: String;
  author: String;
}