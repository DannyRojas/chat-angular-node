import { Component, OnInit, Inject } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders,HttpParams  } from '@angular/common/http';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ChatService} from './../chat.service';

export interface DialogData {
  username: string;
}
@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
    
    myId : string;
    tokenchat: string;
    username: string;
    idfriend: string;
    nameChat: string;
    stateChat: string; 

    data: string[];
    fotoChat: string;
    displayC: string = "DISPLAY";
    displayN: string = "NONE";
    messagess: Messagess[];
    msm :string;
  

  constructor(private chatService: ChatService,public dialog: MatDialog,private http: HttpClient){
   
  }

  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {username: this.username}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.username = result;
      console.log(this.username)
    });
  }
  
  typesOfShoes = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers']
  public friendsx: any = [];
  public messages: any = [];

  //funcion para iniciar 
  ngOnInit() {
    this.friends();
    
  }
  
//preguntar mi lista de amigos
  friends(){
    this.displayC="DISPLAY";
    this.displayN="NONE";
    this.myId =localStorage.getItem('currentUser');
    const body = new HttpParams()
          .set('currentUser',localStorage.getItem('currentUser') );

    this.http.post('/api/v1/friends',body,{
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    })
        .subscribe((data)=>{
            console.log('data success',data)
            if(data['data']!=false){
                var dat = data['json'];
                this.friendsx = (dat);
            }
        },(error) =>{
          console.log('data error',error);
        })
  }
  
  //seleccionar un amigo para iniciar un chat
  SelectChat(id,name,state,foto,myId){
    console.log(id);
    this.displayC="NONE";
    this.displayN="DISPLAY";
    this.idfriend=id;
    this.nameChat=name;
    this.stateChat=state;
    this.fotoChat = foto;
    this.tokenchat = '';
    this.myId = myId;
    const data = [{"id":id,"myid":myId}];
    this.chatService
    .emitMessage(data)
    .subscribe((message: any=[]) => {
      if(this.messages.length>0){
      this.messages[this.messages.length]= {'text':message[0].text,'float':message[0].float,'author':message[0].author,'token_chat':message[0].token_chat};
      }else{
        this.messages = (message);
      }
      console.log(this.messages) 
    });
  }
//enviar un mensaje
  sendMessage(idfriend,myid) {
    this.messagess = 
    [
        {  'text': this.msm, 'author': myid, 'tokenchat':this.messages[0].token_chat,'idfriend':idfriend}
    ];
    this.msm="";
    console.log(this.messagess);
    this.chatService.sendMessage(this.messagess);
   
  }
  
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'modal.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private http: HttpClient){}

    private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    errorSaved: string;
  onNoClick(): void {
    this.dialogRef.close();
  }
  public friendsx: any = [];
  ClickOk(result): void{
    console.log(result);
    if(result!=localStorage.getItem('nametUser')){
      
      const body = new HttpParams()
          .set('currentUser',localStorage.getItem('currentUser'))
          .set('currentFriend',result);

    this.http.post('/api/v1/Addfriend',body,{
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    })
        .subscribe((data)=>{
            console.log('data success',data)
            if(data['data']!=false){
              const body = new HttpParams()
              .set('currentUser',localStorage.getItem('currentUser') );

                  this.http.post('/api/v1/friends',body,{
                    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
                  })
                      .subscribe((data)=>{
                          console.log('data success',data)
                          if(data['data']!=false){
                            var dat = data['json'];
                            this.friendsx = (dat);
                          }
                      },(error) =>{
                        console.log('data error',error);
                      })
              this.dialogRef.close();
            }else{
              this.errorSaved = data['msm'];
            }
        },(error) =>{
          console.log('data error',error);
        })
    }else{
      this.errorSaved = 'Escribe un usuario diferente';
    }
  }

}
export class Messagess {
  idfriend: String;
  text: String;
  author: String;
  tokenchat: String;
}

