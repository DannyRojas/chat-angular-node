import * as $ from 'jquery';
import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders,HttpParams  } from '@angular/common/http';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectorRef,  NgZone, OnDestroy, ViewChild, HostListener, Directive, AfterViewInit } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;    
      
  private _mobileQueryListener: () => void;
  
  
   
    
    constructor(private router : Router ,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,private formBuilder: FormBuilder,private http: HttpClient) {
      this.mobileQuery = media.matchMedia('(min-width: 768px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    username : string
    password : string

  login() : void {
    const body = new HttpParams()
          .set('username', this.username);

    this.http.post('/api/v1/validate',body,{
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    })
        .subscribe((data)=>{
            console.log('data success',data)
            console.log(data['data']);
            
            if(data['data'] != false){
              localStorage.setItem('currentUser',data['id']);
              localStorage.setItem('nametUser',this.username);
              localStorage.setItem('fototUser',data['foto']);
              this.router.navigate(['dashboard']);
            }else{
              alert("este usuario no existe");
            }

        },(error) =>{
          alert("este usuario no existe");
        })
  }

  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  
    ngOnDestroy(): void {
      this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    ngAfterViewInit() {   
    } 

    ngOnInit() {
      if (localStorage.getItem('currentUser')) {
        // logged in so return true
        this.router.navigate(['dashboard']);
    }
      this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    emailFormControl = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
}


